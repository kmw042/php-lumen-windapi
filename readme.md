# Wind API

## Overview

Get the wind speed and cardinal direction by US zip code

## Notes

- As verified by Kevin, cardinal direction is returned not wind direction in degrees
- Currently implemented for US zip codes only
- Returns validation error for invalid zip code format, returns 404 for invalid zip code



## Example Request

http://<your-server>/api/v1/wind/94040

curl --request GET \ --url http://<your-server>/api/v1/wind/94040


## Example Response

{"wind":{"speed":2.1,"direction":"WNW"}}


copyright 2018 kmw042@gmail.com


## Clear the cache

php artisan clear:mycache
