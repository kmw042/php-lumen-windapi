<?php

namespace App\Console\Commands;

use PHPUnit\Framework\TestCase;
use Illuminate\Container\Container;
use App\MockWeatherService as MockWeatherService;
use Illuminate\Support\Facades\Cache;

/**
 * Class BustCacheCommandTest.
 */
class BustCacheCommandTest extends TestCase
{
    protected function setUp()
    {
        $container = Container::getInstance();
        $weatherService = $container->make(MockWeatherService::class);
        /* @noinspection PhpUndefinedFieldInspection */
        $this->weatherObject = $weatherService->getWeather('12345');
    }

    public function testHandle()
    {
        /* @noinspection PhpUndefinedMethodInspection */
        try {
            self::assertNull(Cache::get('12345', null));
        } catch (\Exception $e) {
        }
    }
}
