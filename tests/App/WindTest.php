<?php

namespace App;

use PHPUnit\Framework\TestCase;
use Illuminate\Container\Container;

/**
 * Class WindTest.
 */
class WindTest extends TestCase
{
    private $weatherObject;

    protected function setUp()
    {
        $container = Container::getInstance();
        $weatherService = $container->make(MockWeatherService::class);
        $this->weatherObject = $weatherService->getWeather('12345');
    }

    public function testSetDeg()
    {
        $this->weatherObject->getWind()->setDeg(150.1);
        try {
            self::assertEquals(150.1, $this->weatherObject->GetWind()->getDeg());
        } catch (\Exception $e) {
        }
    }

    public function testSetDirection()
    {
        $this->weatherObject->getWind()->setDirection(150.1);
        try {
            self::assertEquals('NE', $this->weatherObject->GetWind()->getDirection());
        } catch (\Exception $e) {
        }
    }

    public function testSetSpeed()
    {
        $this->weatherObject->getWind()->setSpeed(150.1);
        try {
            self::assertEquals(150.1, $this->weatherObject->GetWind()->getSpeed());
        } catch (\Exception $e) {
        }
    }
}
