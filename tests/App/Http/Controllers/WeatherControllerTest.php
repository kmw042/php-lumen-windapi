<?php

namespace App\Http\Controllers;

use PHPUnit\Framework\TestCase;
use Illuminate\Container\Container;
use App\MockWeatherService as MockWeatherService;
use App\Wind as Wind;

/**
 * Class WeatherControllerTest.
 */
class WeatherControllerTest extends TestCase
{
    private $weatherObject;

    protected function setUp()
    {
        $container = Container::getInstance();
        $weatherService = $container->make(MockWeatherService::class);
        $this->weatherObject = $weatherService->getWeather('12345');
    }

    public function testShowWind()
    {
        try {
            self::assertInstanceOf(Wind::class, $this->weatherObject->getWind());
        } catch (\Exception $e) {
        }
    }
}
