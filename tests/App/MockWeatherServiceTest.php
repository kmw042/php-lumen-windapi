<?php

namespace App;

use PHPUnit\Framework\TestCase;
use Illuminate\Container\Container;

class MockWeatherServiceTest extends TestCase
{
    private $weatherService;
    private $weatherObject;

    protected function setUp()
    {
        $container = Container::getInstance();
        $this->weatherService = $container->make(MockWeatherService::class);
        $this->weatherObject = $this->weatherService->getDisplayWind('12345');
    }

    public function test__construct()
    {
        Self::assertInstanceOf(WeatherServiceCacheDecorator::class, new WeatherServiceCacheDecorator());
    }

    public function testValidate()
    {
        try {
            self::assertNull($this->weatherService->validate(94040));
        } catch (\Exception $e) {
        }
    }

    public function testGetWind()
    {
        try {
            self::assertArraySubset(
                ['wind' => ['speed' => 4.47, 'direction' => 'NE']],
                $this->weatherObject
            );
        } catch (\Exception $e) {
        }
    }
}
