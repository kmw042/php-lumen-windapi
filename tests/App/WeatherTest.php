<?php

namespace App;

use Illuminate\Container\Container;
use PHPUnit\Framework\TestCase;

/**
 * Class WeatherTest.
 */
class WeatherTest extends TestCase
{
    private $weatherObject;

    protected function setUp()
    {
        $container = Container::getInstance();
        $weatherService = $container->make(MockWeatherService::class);
        $this->weatherObject = $weatherService->getWeather('12345');
    }

    public function testGetDisplayWind()
    {
        try {
            self::assertArraySubset(
                ['wind' => ['speed' => 4.47, 'direction' => 'NE']],
                [
                    'wind' => array(
                        'speed' => $this->weatherObject->getWind()->getSpeed(),
                        'direction' => $this->weatherObject->getWind()->getDirection(),
                    ),
                ]
            );
        } catch (\Exception $e) {
        }
    }

    public function testSetWind()
    {
        $this->weatherObject->setWind(new Wind());
        try {
            self::assertInstanceOf(Wind::class, $this->weatherObject->getWind());
        } catch (\Exception $e) {
        }
    }

//    public function testSetBase()
//    {

//    }

//    public function testSetClouds()
//    {

//    }

//    public function testSetCoord()
//    {

//    }

//     public function testSetMain()
//    {

//    }
// public function testSetName()
//    {

//    }

//  public function testSetPressure()
//    {

//    }
//   public function testSetTemp()
//    {

//    }

//    public function testSetWeather()
//    {

//    }
}
