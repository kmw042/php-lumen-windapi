<?php

namespace App\Http\Controllers;

use App\WeatherServiceInterface;

/**
 * Class WeatherController.
 */
class WeatherController extends Controller
{
    private $service;

    /**
     * Create a new controller instance.
     *
     * @param WeatherServiceInterface $service
     */
    public function __construct(WeatherServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @param $zipCode
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function showWind($zipCode)
    {
        return response()->json($this->service->getDisplayWind($zipCode));
    }
}
