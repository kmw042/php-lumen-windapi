<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class WeatherServiceProvider.
 */
class WeatherServiceProvider extends ServiceProvider
{
    /**
     * Register application services.
     */
    public function register()
    {
        $this->app->bind('App\WeatherServiceInterface', 'App\WeatherServiceCacheDecorator');

        $this->app->bind('App\WeatherWrapper', 'App\OpenWeatherMapService');
    }
}
