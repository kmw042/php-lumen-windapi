<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\Cache;
use Illuminate\Console\Command;

/**
 * Class BustCacheCommand.
 *
 * Implementation of command to bust the cache
 */
class BustCacheCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'clear:mycache';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear the cache.';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        Cache::flush();
        echo 'Cache has been cleared.';
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }
}
