<?php

namespace App;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;

/**
 * Description of OpenWeatherMapService.
 *
 * @author a42
 */
class OpenWeatherMapService extends WeatherWrapper implements WeatherServiceInterface
{
    public static $baseUri = 'https://api.openweathermap.org/data/2.5/';
    private $appId = '6efbff6c6e97ecab6ddadbb0af380d41';

    public function getDisplayWind($zipCode)
    {
        $weather = $this->getWeather($zipCode);

        return $weather->getDisplayWind();
    }

    public function getWeather($zipCode) : Weather
    {
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer(null, null, null, new ReflectionExtractor()));

        $serializer = new Serializer($normalizers, $encoders);

        $client = new Client(['base_uri' => self::$baseUri]);

        $request = new Request('GET', "weather?zip=$zipCode&appid=$this->appId");
        $response = $client->send($request, ['timeout' => 2]);

        $weatherObject = $serializer->deserialize($response->getBody(), Weather::class, 'json');

        return $weatherObject;
    }
}
