<?php

namespace App;

use Illuminate\Container\Container;
use Illuminate\Support\Facades\Cache;

/**
 * Create a new class instance.
 *
 * @param WeatherService $service
 *
 * @return array
 */
class WeatherServiceCacheDecorator implements WeatherServiceInterface
{
    private $weatherService;
    private $cacheTime = 15;

    /**
     * WeatherServiceCacheDecorator constructor.
     */
    public function __construct()
    {
        $container = Container::getInstance();
        $this->weatherService = $container->make(WeatherWrapper::class);
    }

    /**
     * @param $zipCode
     *
     * @return mixed
     */
    public function getDisplayWind($zipCode)
    {
        $this->validate($zipCode);

        return Cache::remember($zipCode, $this->cacheTime, function () use ($zipCode) {
            return $this->weatherService->getDisplayWind($zipCode);
        });
    }

    /**
     * @param $zip
     *
     * @return mixed|void
     */
    public function validate($zip)
    {
        $this->weatherService->validate($zip);
    }
}
