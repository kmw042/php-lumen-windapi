<?php
/**
 * Created by PhpStorm.
 * User: a42
 * Date: 2018-12-05
 * Time: 10:16.
 */

namespace App;

abstract class WeatherWrapper implements WeatherServiceInterface
{
    abstract public function getWeather($zipCode) : Weather;
    abstract public function getDisplayWind($zipCode);
    /**
     * @param $zip
     *
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function validate($zip)
    {
        $rules = array(
            'zipCode' => 'required|integer|size:5',
        );

        $messages = array(
            'zipCode.required' => 'zipCode is required.',
        );
        $validator = \Validator::make(array('zipCode' => $zip), $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();

            return response()->json($errors->all());
        }
    }
}
