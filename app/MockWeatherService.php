<?php

/*
 *  Class name
 *
 *  This class ...
 *
 *  @input - varname	desc
 *
 *  kmw042 �2018
 */

namespace App;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;

/**
 * Class MockWeatherService.
 *
 * @author a42
 */
class MockWeatherService extends WeatherWrapper implements WeatherServiceInterface
{
    /**
     * @var string
     */
    public $mockWeather = '{
  "coord":
  {
    "lon": -112.08,
    "lat": 33.45
  },
  "sys":
  {
    "message": 0.338,
    "country": "United States of America",
    "sunrise": 1400156840,
    "sunset": 1400206919
  },
  "weather": [
  {
    "id": 800,
    "main": "Clear",
    "description": "Sky is Clear",
    "icon": "01d"
  }],
  "base": "cmc stations",
  "main":
  {
    "temp": 74.53,
    "temp_min": 74.53,
    "temp_max": 74.53,
    "pressure": 978.87,
    "sea_level": 1033.54,
    "grnd_level": 978.87,
    "humidity": 20
  },
  "wind":
  {
    "speed": 4.47,
    "deg": 45
  },
  "clouds":
  {
    "all": 0
  },
  "dt": 1400167430,
  "id": 5308655,
  "name": "Phoenix",
  "cod": 200
}';

    /**
     * @param $zipCode
     *
     * @return array|mixed
     */
    public function getDisplayWind($zipCode)
    {
        $weather = $this->getWeather($zipCode);

        return $weather->getDisplayWind();
    }

    /**
     * @param $zipCode
     *
     * @return Weather
     */
    public function getWeather($zipCode) : Weather
    {
        $stream = fopen('php://memory', 'r+');
        fwrite($stream, $this->mockWeather);
        rewind($stream);

        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer(null, null, null, new ReflectionExtractor()));
        $serializer = new Serializer($normalizers, $encoders);

        $weatherObject = $serializer->deserialize(stream_get_contents($stream), Weather::class, 'json');

        return $weatherObject;
    }
}
