<?php

namespace App;

/**
 * Interface WeatherServiceInterface.
 */
interface WeatherServiceInterface
{
    /**
     * @param $zipCode
     *
     * @return mixed
     */
    public function getDisplayWind($zipCode);

    /**
     * @param $zip
     *
     * @return mixed
     */
    public function validate($zip);
}
