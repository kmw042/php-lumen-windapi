<?php

namespace App;

/**
 * Class Wind.
 */
class Wind
{
    private $deg;
    private $direction;
    private $directions = array('N', 'NNE', 'NE', 'ENE', 'E', 'ESE', 'SE', 'SSE', 'S', 'SSW', 'SW', 'WSW', 'W', 'WNW', 'NW', 'NNW');
    private $speed;

    /**
     * @return array
     */
    public function getDisplayWind()
    {
        $windArray = array('wind' => array('speed' => $this->getSpeed(), 'direction' => $this->getDirection()));

        return $windArray;
    }

    /**
     * @return mixed
     */
    public function getDeg()
    {
        return $this->deg;
    }

    /**
     * @param $deg
     */
    public function setDeg($deg)
    {
        $this->deg = $deg;
        $this->setDirection($deg);
    }
    /**
     * @return mixed
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * @param $degrees
     */
    public function setDirection($degrees)
    {
        $dir_degrees = intval(($degrees / 22.5) + .5);
        $index = $dir_degrees % 16;
        $this->direction = $this->directions[$index];
    }

    /**
     * @return mixed
     */
    public function getSpeed()
    {
        return $this->speed;
    }

    /**
     * @param $speed
     */
    public function setSpeed($speed)
    {
        $this->speed = $speed;
    }
}
