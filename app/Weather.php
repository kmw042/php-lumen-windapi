<?php

namespace App;

/**
 * Class Weather.
 */
class Weather //implements WeatherService
{
    private $base;
    private $clouds;
    private $coord;
    private $main;
    private $name;
    private $pressure;
    private $temp;
    private $weather;
    private $wind;

    /**
     * @return array
     */
    public function getDisplayWind()
    {
        return $this->getWind()->getDisplayWind();  //$windArray; //$this->wind;
    }

    public function getBase()
    {
        return $this->base;
    }

    /**
     * @param $base
     */
    public function setBase($base)
    {
        $this->base = $base;
    }

    public function getClouds()
    {
        return $this->clouds;
    }

    /**
     * @param $clouds
     */
    public function setClouds($clouds)
    {
        $this->clouds = $clouds;
    }
    public function getCoord()
    {
        return $this->coord;
    }

    /**
     * @param $coord
     */
    public function setCoord($coord)
    {
        $this->coord = $coord;
    }

    public function getMain()
    {
        return $this->main;
    }

    /**
     * @param $main
     */
    public function setMain($main)
    {
        $this->main = $main;
    }

    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    public function getPressure()
    {
        return $this->pressure;
    }

    /**
     * @param $pressure
     */
    public function setPressure($pressure)
    {
        $this->pressure = $pressure;
    }

    public function getTemp()
    {
        return $this->temp;
    }

    /**
     * @param $temp
     */
    public function setTemp($temp)
    {
        $this->temp = $temp;
    }
    public function getWeather()
    {
        return $this->weather;
    }

    /**
     * @param $weather
     */
    public function setWeather($weather)
    {
        $this->weather = $weather;
    }

    public function getWind()
    {
        return $this->wind;
    }

    /**
     * @param Wind $wind
     */
    public function setWind(Wind $wind)
    {
        $this->wind = $wind;
    }
}
